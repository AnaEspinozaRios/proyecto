import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as fromComponents from './components'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http'


// b = {a:1,c:2,b:3} -> fromComponents.components
// a = {d:1,r:1}
///// c = ...a, ...b
////  c = {a:1,c:2,b:3,d:1,r:1}

@NgModule({
  imports: [
    FormsModule,// para los formularios
    ReactiveFormsModule,//variables reativas 
    HttpClientModule,//para comunicardos con el restapi -> GO -> backend
    CommonModule
  ],
  declarations: [
    //fromComponents.components -> {a:1,c:2,b:3}
    //...fromComponents.components -> a:1,c:2,b:3
    ...fromComponents.components
  ],
  exports:[
    FormsModule,// para los formularios
    ReactiveFormsModule,//variables reativas 
    HttpClientModule,//para comunicardos con el restapi -> GO -> backend
    CommonModule,
    ...fromComponents.components
  ]
})


export class SharedModule { }
