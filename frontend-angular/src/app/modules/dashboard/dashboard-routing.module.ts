import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';


const routes: Routes = [
    {
  path:'',//principal
  component:HomeDashboardComponent
},

];
//{useHash:true} -> quita el # de la url
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }