import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar-dashboard',
  templateUrl: './navbar-dashboard.component.html',
  styleUrls: ['./navbar-dashboard.component.css']
})
export class NavbarDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
public openCloseSidebar(){
  document.body.classList.toggle('sb-sidenav-toggled');
}
}
