import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticiasPublicationsComponent } from './noticias-publications.component';

describe('NoticiasPublicationsComponent', () => {
  let component: NoticiasPublicationsComponent;
  let fixture: ComponentFixture<NoticiasPublicationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoticiasPublicationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticiasPublicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
