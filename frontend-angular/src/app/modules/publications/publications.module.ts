import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPublicationsComponent } from './list-publications/list-publications.component';
import { CreatePublicationsComponent } from './create-publications/create-publications.component';
import { SharedModule } from '@shared/shared.module';
import { PublicationsRoutingModule } from './publications-routing.module';
import { VideosPublicationsComponent } from './videos-publications/videos-publications.component';
import { NoticiasPublicationsComponent } from './noticias-publications/noticias-publications.component';



@NgModule({
  declarations: [
    ListPublicationsComponent,
    CreatePublicationsComponent,
    VideosPublicationsComponent,
    NoticiasPublicationsComponent
  ],
  imports: [
    SharedModule,
    PublicationsRoutingModule
  ]
})
export class PublicationsModule { }
