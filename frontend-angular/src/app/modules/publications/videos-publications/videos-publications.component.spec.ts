import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideosPublicationsComponent } from './videos-publications.component';

describe('VideosPublicationsComponent', () => {
  let component: VideosPublicationsComponent;
  let fixture: ComponentFixture<VideosPublicationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideosPublicationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideosPublicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
