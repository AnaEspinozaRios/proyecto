import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatePublicationsComponent } from './create-publications/create-publications.component';
import { ListPublicationsComponent } from './list-publications/list-publications.component';
import { NoticiasPublicationsComponent } from './noticias-publications/noticias-publications.component';
import { VideosPublicationsComponent } from './videos-publications/videos-publications.component';


const routes: Routes = [
    {
  path:'create-publications',
  component:CreatePublicationsComponent
},
{
  path:'list-publications',
  component:ListPublicationsComponent
},
{
  path:'noticias-publications',
  component:NoticiasPublicationsComponent
},
{
  path:'videos-publications',
  component:VideosPublicationsComponent
},

];
//{useHash:true} -> quita el # de la url
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicationsRoutingModule { }